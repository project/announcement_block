CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Announcement Block module adds a block titled "Announcement Block" that
can be given specific content and colours by whoever has the permission to do so.

There are similar modules in the ecosystem but this was developed
specifically to allow for colour customisation of the block
by the editors of the site, as well as allowing a countdown to be included
in the text if needed.

 * For a full description of the module visit:
   https://www.drupal.org/project/announcement_block

 * To submit bug reports and feature suggestions, or to track changes
   visit: https://www.drupal.org/project/issues/announcement_block


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Announcement Block module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Announcement Block module.
    2. Navigate to Administration > Structure > Block and select which region
       and select "Place block" again.
    3. Enter the details, noting there are specific tokens available for the countdown.
    6. Restrict if needed to content types, pages, roles, etc. The region can also be changed.
       Save block.


MAINTAINERS
-----------

 * Maxwell Keeble (maxwellkeeble) - https://www.drupal.org/u/maxwellkeeble
