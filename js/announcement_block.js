
/**
 * @file
 *
 *
 */

(function ($, Drupal) {
  Drupal.behaviors.runCountdown = {
    attach: function (context, settings) {
      'use strict';

      $(document).ready(function () {
        if ($('#announcement_block_countdown').length > 0) {
          var timestamp = $('.announcement_block').attr('timestamp');
          if (timestamp) {
            var x = setInterval(function () {
              $('#announcement_block_countdown').html(countdown(parseInt(timestamp) * 1000).toString());
            }, 1000);
          }
        }
      });

    }
  };
}(jQuery, Drupal));

